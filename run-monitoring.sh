#!/usr/bin/env bash

echo '<<< MONITORING-SERVER STARTED >>>' >&2

RUNNING_MODE_WEB_APP="WEB_APP"
RUNNING_MODE_WORKER="WORKER"



function init() {
    # инициализация из переменных окружения
    if [[ -z $RUNNING_MODE ]]
    then
        RUNNING_MODE=$RUNNING_MODE_WEB_APP  # default
    fi
    if [[ -z $DEBUG ]]
    then
        DEBUG="FALSE"
    fi
    DEBUG_MODE=`echo "${DEBUG}" | tr a-z A-Z`
    RUNNING_MODE=`echo "${RUNNING_MODE}" | tr a-z A-Z`
}


init
export PYTHONPATH=$(pwd)
echo "RUNNING_MODE: ${RUNNING_MODE}"

if [ "${DEBUG_MODE}" = "TRUE" ]
then
    echo "warning: mode debug" >&2
    sleep 1000000000000
else
    case "${RUNNING_MODE}" in
        "${RUNNING_MODE_WEB_APP}")
            python3 ./monitoring/app.py
            ;;
        "${RUNNING_MODE_WORKER}")
            python3 ./monitoring/worker.py
            ;;
        *)
            echo "CMD ERROR" >&2
            ;;
    esac
fi
