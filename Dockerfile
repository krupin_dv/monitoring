FROM python:3.8-slim

# TZ Settings
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# copy and install requirements separately to not do this every time on build
COPY ./requirements.txt /
RUN pip install -r /requirements.txt --no-cache-dir -q

ENV BASE_DIR /src/monitoring-service
ENV PYTHONPATH $BASE_DIR
COPY ./ $BASE_DIR
WORKDIR $BASE_DIR
RUN cd $BASE_DIR

RUN mkdir -p /log/

CMD ./run-monitoring.sh
#CMD ["python3", "./monitoring/app.py"]
