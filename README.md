## Установка (общая)
Для работы нужно установить redis

`sudo apt-get update`

`sudo apt-get install redis-server`


## Локально
git clone git@gitlab.vizorlabs.ru:video/monitoring-service.git

`cd monitoring-service`

`pip install -r requirements.txt`

`export PYTHONPATH=$(pwd)`

`python monitoring/app.py`
```
settings loaded
BASE_DIR:  /home/zet/ws/monitoring
======== Running on http://0.0.0.0:8080 ========
(Press CTRL+C to quit)
```

Запуск теста производительности
`pytest monitoring/tests/perfomance_test.py::test_max_connections_real_server --capture=no`



## Докер
`docker build . -t monitoring`

`docker run -p 0.0.0.0:8080:8080 --env-file .env --net host monitoring`


## Ипользование
см. примеры в run_test_request.sh

`curl -X POST http://0.0.0.0:8080/api/camera/123/component/inference/detected/ -d '{"fps":29.5, "gpuid": 1}'`
>{"id": 123, "inference": {"fps": 29.5, "gpuid": 1, "detected_at": "2019-11-18T14:12:55.367778"}}

`curl http://0.0.0.0:8080/api/camera/`
>{"count": 1, "COMPONENT_LIFE_TIME seconds": 3600.0, "result": [{"id": 123, "inference": {"fps": 29.5, "gpuid": 1, "detected_at": "2019-11-19T17:21:02.296731", "is_alive": true}}]}


`curl http://127.0.0.1:8080/api/camera/inference/aggregate-gpu/`
>{"cam_count": 1, "gpu_count": 1, "no_gpu_count": 0, "result": {"1": [{"fps": 29.5, "gpuid": 1, "detected_at": "2019-11-19T17:23:12.288028", "is_alive": true, "id": 123}]}, "no_gpu_result": [], "COMPONENT_LIFE_TIME seconds": 3600.0}

`curl -X DELETE http://0.0.0.0:8080/api/camera/test123/`
>{"deleted_records": 1}

`curl http://0.0.0.0:8080/api/camera/`
>{"count": 0, "result": []}


## Запуск тестов
`pip install -r requirements_test.txt`

`export REDIS_HOST=127.0.0.1`
`export PYTHONPATH=$(pwd)`

`pytest  monitoring/tests/test_*`

или

`pytest monitoring/tests/test_views.py --capture=no`
