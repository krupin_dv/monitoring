#!/usr/bin/env bash

HOST='http://0.0.0.0:8080'
echo

curl -X POST "$HOST/api/camera/123/component/mjpeg/detected/" -d '{"fps":29.5}'
# {"id": 123, "mjpeg": {"fps": 29.5, "detected_at": "2019-11-18T14:12:55.367778"}}
echo

curl "$HOST/api/camera/"
# {"count": 1, "COMPONENT_LIFE_TIME seconds": 3600.0, "result": [{"id": 123, "mjpeg": {"fps": 29.5, "detected_at": "2019-11-18T14:13:38.707570", "is_alive": true}}]}
echo

curl -X DELETE "$HOST/api/camera/123/"
# {"deleted_records": 1}
echo

curl "$HOST/api/camera/"
# {"count": 0, "result": []}
echo
