import re
import time
import json
import logging
from datetime import datetime
from collections import defaultdict

from aiohttp import web
from aiohttp import web_response

from settings import settings
from monitoring.db import get_db, redis_set, redis_get, redis_delete
from monitoring.component_status import component_update_signal, get_events

log = logging.getLogger('monitoring.views')


COMPONENTS = settings.COMPONENTS

CAMERA_COMPONENT_PATTERN = '{component}-{cam_id}'
CAMERA_COMPONENT_PATTERN_RE = re.compile(r'^(?P<component>[\w\-]+)-(?P<cam_id>\d+)$', re.IGNORECASE)

FIELDS_FOR_CONVERT = {
    'id': int,
    'gpuid': int,
    'fps': float
}


def parse_key(key):
    # в ключе camera-mjpeg-123 cam_id - в конце
    mo = CAMERA_COMPONENT_PATTERN_RE.match(key)
    if mo is None:
        log.error(f'Error to parse key `{key}`')
        return None, None
    component = mo['component']
    cam_id = int(mo['cam_id'])
    return component, cam_id


def is_alive(component: dict, now=None, add_field_to_dict=True):
    if now is None:
        now = time.time()

    if not 'ts' in component:
        return
    component['ts'] = float(component['ts'])
    is_alive = component['ts'] >= now - settings.COMPONENT_LIFE_TIME

    if add_field_to_dict:
        component['is_alive'] = is_alive
        component['detected_at'] = datetime.utcfromtimestamp(component['ts']).isoformat()

    return is_alive


async def component_data_fix(component_data):
    """ задаём корректные типы"""
    is_alive(component_data)

    for field, foo in FIELDS_FOR_CONVERT.items():
        v = component_data.get(field)
        if v:
            component_data[field] = foo(v)


async def inference_aggregate_gpu(request):
    redis = await get_db()
    pattern = '*'
    keys = await redis.keys(pattern)

    cameras = {}
    for key in keys:
        component_name, cam_id = parse_key(key)
        if component_name not in COMPONENTS:  # old component or some unexpected key in DB
            continue
        component_data = await redis.hgetall(key)

        camera = cameras.get(cam_id, {})
        camera['id'] = cam_id
        await component_data_fix(component_data)
        camera[component_name] = component_data
        # пробрасываем resolution наверх
        resolution = component_data.get('resolution')
        if resolution:
            camera['resolution'] = resolution

        cameras[cam_id] = camera

    # получаем словарь
    gpuid_aggregation = defaultdict(list)  # {gpu_id1: cam_list1, gpu_id2: cam_list2}
    no_gpu_list = []  # cписок камер без gpu

    for cam_id, camera in cameras.items():
        inference = camera.get('inference', {})
        inference['id'] = camera['id']  # add cam_id to dict
        gpuid = inference.get('gpuid')
        if gpuid:
            gpuid_aggregation[gpuid].append(inference)
        else:
            no_gpu_list.append(inference)

    resp = {
        'cam_count': len(cameras),
        'gpu_count': len(gpuid_aggregation),
        'no_gpu_count': len(no_gpu_list),
        'result': gpuid_aggregation,
        'no_gpu_result': no_gpu_list,
        'COMPONENT_LIFE_TIME seconds': settings.COMPONENT_LIFE_TIME,
    }
    return web.json_response(resp)


async def get_statuses(request):
    redis = await get_db()
    pattern = '*'
    keys = await redis.keys(pattern)

    cameras = {}
    for key in keys:
        component_name, cam_id = parse_key(key)
        if component_name not in COMPONENTS:  # old component or some unexpected key in DB
            continue
        component_data = await redis.hgetall(key)

        camera = cameras.get(cam_id, {})
        camera['id'] = cam_id
        await component_data_fix(component_data)
        camera[component_name] = component_data
        # пробрасываем resolution наверх
        resolution = component_data.get('resolution')
        if resolution:
            camera['resolution'] = resolution

        cameras[cam_id] = camera

    cameras_list = list(cameras.values())

    resp = {'count': len(cameras_list), 'COMPONENT_LIFE_TIME seconds': settings.COMPONENT_LIFE_TIME, 'result': cameras_list}
    return web.json_response(resp)


async def update_status(request):
    redis = await get_db()

    cam_id = int(request.match_info['cam_id'])
    component = request.match_info['component']  # mjpeg, inference ...
    log.info(f'Get update_status request from `{request.remote}` for camera `{cam_id}` component `{component}`')

    try:
        data = await request.json()
        log.debug('data: %s' % data)
    except json.decoder.JSONDecodeError:
        log.error('error: data JSONDecodeError')
        data = {}

    # validation
    errors = {}
    for k, foo in FIELDS_FOR_CONVERT.items():
        if k in data:
            try:
                #data[k] = foo(data[k])  # не нужно преобразовавывать, потому что в редисе всё сохранится как строка
                foo(data[k])  # пытаемся преобразовать
            except ValueError:
                errors[k] = 'Wrong value, should be %s' % str(foo)
    if errors:
        return web.json_response({'errors': errors}, status=400)
    # validation

    key = CAMERA_COMPONENT_PATTERN.format(cam_id=cam_id, component=component)
    ts = time.time()
    data['ts'] = ts
    res = await redis.hmset_dict(key, data)
    await redis.expire(key, settings.REDIS_EXPIRE)

    await component_update_signal(cam_id, component, ts)
    return web.json_response(data)


async def cam_delete_view(request):
    redis = await get_db()

    cam_id = request.match_info['cam_id']
    log.info(f'Get camera delete request from `{request.remote}` for camera `{cam_id}`')

    deleted_records = 0
    for component in settings.COMPONENTS:
        key = CAMERA_COMPONENT_PATTERN.format(cam_id=cam_id, component=component)
        r = await redis.delete(key)
        deleted_records += r

    return web.json_response({'deleted_records': deleted_records})


async def component_event_views(request):

    component = request.match_info['component']  # mjpeg, inference ...
    if component not in settings.COMPONENTS:
        return web.json_response({'error': f'Invalid `component` passed value `{component}`'}, status=400)

    id = request.query.get('id')
    if not id:
        return web.json_response({'error': '`id` param required'}, status=400)

    try:
        cam_ids = json.loads(id)
    except json.decoder.JSONDecodeError:
        return web.json_response({'error': '`id` cannot be decoded. Value `{}`'.format(id)}, status=400)

    if isinstance(cam_ids, int):  # '1' it's valid json string, so we get 1 after json.loads
        cam_ids = [cam_ids]

    result = {}
    for cam_id in cam_ids:
        res = await get_events(cam_id, component)
        result[cam_id] = res

    return web.json_response(result)
