import asyncio

import aioredis

from settings import settings
from monitoring.db import get_db, redis_set, redis_get


async def test_redis(loop):
    redis = await aioredis.create_redis_pool((settings.REDIS_HOST, settings.REDIS_PORT), encoding='utf-8')

    assert await redis.exists('my-key') == 0

    await redis.set('my-key', 'value')

    assert await redis.exists('my-key') == 1
    val = await redis.get('my-key')
    print(val)
    assert val == 'value'

    await redis.delete('my-key')  # manually delete
    assert await redis.exists('my-key') == 0


async def test_redis_1(loop):
    await redis_set('key1', 'val-1')
    val = await redis_get('key1')
    print(val)
    assert val == 'val-1'


async def test_redis_2(loop):
    data = {'abcd': 123}
    await redis_set('key1', data)
    val = await redis_get('key1')
    print(val)
    assert val == data


async def test_expire_setting(loop):
    await redis_set('k', 'v')
    val = await redis_get('k')
    assert val == 'v'

    settings.REDIS_EXPIRE = 1
    await redis_set('k', 'v')
    await asyncio.sleep(1)
    val = await redis_get('k')
    assert val is None


async def test_expire(loop):
    await redis_set('k', 'v')
    val = await redis_get('k')
    assert val == 'v'

    settings.REDIS_EXPIRE = 1
    await redis_set('k', 'v', expire=1)
    await asyncio.sleep(1)
    val = await redis_get('k')
    assert val is None


async def test_hset(loop):
    redis = await get_db()

    key = 'my-dict-1'
    d = {
        'key1': 'val1',
        'key2': 'val2',
        #'key3': {'key3-1', 'val3-1'},  # can't save  nested dict
        'gpuid': 145,  # sad what it will be saved as string...
    }

    res = await redis.hmset_dict(key, d, expire=2)
    print(res)

    data = await redis.hgetall(key)
    print(data)

    # find hash
    keys = await redis.keys(key)
    print(keys)

    # set expire
    await redis.expire(key, 1)
    await asyncio.sleep(1)
    keys = await redis.keys(key)
    assert keys == []
