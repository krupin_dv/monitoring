from pprint import pprint

from monitoring.camera import update_cameras_information_task, update_camera, get_camera


async def test_update_cameras_information_worker(aiohttp_client, loop):
    pass
    #data = await update_cameras_information_task()


async def test_update_camera(aiohttp_client, loop):
    data = {  # data to save
        'id': 1,
        'name': 'Камера 1',
        'address': 'Some address',
        'rtsp_uri_main': 'rtsp://172.16.0.92/',
    }
    camera = await update_camera(data)
    assert camera is not None

    cam_in_db = await get_camera(camera.id)
    assert camera.id == cam_in_db.id
    assert camera.name == cam_in_db.name
    assert camera.address == cam_in_db.address
    assert camera.rtsp_uri_main == cam_in_db.rtsp_uri_main
    #pprint(cam_in_db)


async def test_get_camera(aiohttp_client, loop):
    pass


async def test_get_camera_when_not_exists(aiohttp_client, loop):
    camera = await get_camera(123456789)
    assert camera is None
