import time
import asyncio
from pprint import pprint

import pytest
from aiohttp import web
from aiohttp import ClientSession

from settings import settings
from monitoring.app import get_app


async def test_max_connections(aiohttp_client, loop):
    async def post_request(aiohttp_client):
        app = get_app()
        client = await aiohttp_client(app)
        url = app.router['update_status'].url_for(cam_id='1', component='mjpeg')

        data = {'resolution': '1280x760'}
        resp = await client.post(url, json=data)
        if not resp.status == 200:
            print('error >>>>>>>>>')
            print(await resp.text())
            return

        assert resp.status == 200
        data = await resp.json()
        #print('ok >>>>>>>>>')
        return 'ok'

    REQUESTS_NUMBER = 1000
    print(f'REQUESTS_NUMBER: {REQUESTS_NUMBER}')

    tasks = []
    for i in range(REQUESTS_NUMBER):
        coro = post_request(aiohttp_client)
        tasks.append(coro)


    #res = await asyncio.gather(*tasks)
    done, pending = await asyncio.wait(tasks)
    #print(res)
    res = [t.result() for t in done]
    assert all(res)


async def test_max_connections_real_server(aiohttp_client, loop):
    async def post_request(aiohttp_client, i):
        cam_id = (i % 10) + 1  # from 1 to 40
        component = settings.COMPONENTS[i % 3]  # get 0-2 element by index
        data = {'resolution': '1280x760'}
        url = f'http://0.0.0.0:8080/api/camera/{cam_id}/component/{component}/detected/'

        async with ClientSession() as session:
            async with session.post(url, json=data) as resp:
                if not resp.status == 200:
                    print('error >>>>>>>>>')
                    print(await resp.text())
                else:
                    #print('ok >>>>>>>>>')
                    return 'ok'

    REQUESTS_NUMBER = 1000
    print()
    print(f'REQUESTS_NUMBER: {REQUESTS_NUMBER}')

    tasks = []
    for i in range(REQUESTS_NUMBER):
        coro = post_request(aiohttp_client, i)
        tasks.append(coro)

    #res = await asyncio.gather(*tasks)
    start = time.time()
    done, pending = await asyncio.wait(tasks)
    taken = time.time() - start
    print(f'time taken: {taken}')

    #print(res)
    res = [t.result() for t in done]
    assert all(res)
