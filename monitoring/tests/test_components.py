import asyncio
import time
from pprint import pprint

import pytest

from settings import settings
from monitoring.db import get_db
from monitoring.component_status import COMPONENT_STATE_DB, COMPONENT_ALIVE_PATTERN
from monitoring.component_status import component_update_signal, remove_not_alived_elements
from monitoring.component_status import write_event, get_events, remove_old_events


@pytest.fixture
async def component_events():
    cam_id = 1
    component = 'inference'
    ts1 = 1600123456.1234567  # 7 digits in float part
    ts2 = 1600123457.0  # 0 digits in float part
    ts3 = 1600123458.123456  # 6 digits in float part

    # message_id(ts), event_data
    events = [
        (ts1, {'is_alive': 1}),
        (ts2, {'is_alive': 0}),
        (ts3, {'is_alive': 1}),
    ]

    # записываем данные в БД
    for ts, event_data in events:
        res = await write_event(cam_id, component, ts, event_data)

    return events


async def test_component_update_signal(loop):
    res = await component_update_signal(cam_id=1, component='inference', ts=123.0)
    assert res == 1  # проверяем что произошло добавление

    res = await component_update_signal(cam_id=1, component='inference', ts=123.0)
    assert res == 0  # нет добавления

    res = await component_update_signal(cam_id=1, component='inference', ts=123456.0)
    assert res == 0  # нет добавления


async def test_remove_not_alived_elements(loop):
    redis = await get_db(db=COMPONENT_STATE_DB)

    ts_treshold = time.time() - settings.COMPONENT_LIFE_TIME

    # добавим мертвый компонент
    component = 'inference'
    res = await component_update_signal(1, component, ts=ts_treshold - 600)
    assert res == 1  # проверяем что произошло добавление
    # добавим живой компонент
    res = await component_update_signal(2, component, ts=ts_treshold + 600)
    assert res == 1  # проверяем что произошло добавление

    key = COMPONENT_ALIVE_PATTERN.format(component=component)
    elements = await redis.zrange(key)
    assert elements == ['1', '2']

    removed_elements = await remove_not_alived_elements(component)
    assert len(removed_elements) == 1
    assert removed_elements[0][0] == '1'

    # check db
    score = await redis.zscore(key, 1)
    assert score is None  # None потому что элемент был удалён

    score = await redis.zscore(key, 2)
    assert score is not None


async def test_write_event(loop):
    cam_id=1
    component = 'inference'
    ts = 1600111985.9823850  # zero at end
    event_data = {'is_alive': 0}

    res = await write_event(cam_id, component, ts, event_data)
    assert res == '16001119859823850-0'


async def test_get_events(loop, component_events):
    cam_id=1
    component = 'inference'
    ts1 = 1600123456.1234567  # 7 digits in float part
    ts2 = 1600123457.0  # 0 digits in float part
    ts3 = 1600123458.123456  # 6 digits in float part

    res = await get_events(cam_id, component)
    # проверяем результат
    assert len(res) == 3
    assert res[0]['ts'] == component_events[0][0]
    assert res[1]['ts'] == component_events[1][0]
    assert res[2]['ts'] == component_events[2][0]
    assert res[0]['is_alive'] == component_events[0][1]['is_alive']
    assert res[1]['is_alive'] == component_events[1][1]['is_alive']
    assert res[2]['is_alive'] == component_events[2][1]['is_alive']


async def test_remove_old_events(loop):

    cam_id = 1
    component = 'inference'

    ts_treshold = time.time() - settings.COMPONENT_EVENT_EXPIRE

    old_ts = ts_treshold - 1
    new_ts = ts_treshold + 1000

    # добавим данные
    res = await write_event(cam_id, component, old_ts, {'is_alive': 1})
    res = await write_event(cam_id, component, new_ts, {'is_alive': 0})
    res = await get_events(cam_id, component)
    assert len(res) == 2

    deleted_count = await remove_old_events()
    assert deleted_count == 1

    res = await get_events(cam_id, component)
    assert len(res) == 1
    assert res[0]['ts'] == new_ts
