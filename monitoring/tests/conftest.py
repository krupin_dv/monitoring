import os
import asyncio
import pytest

os.environ['SETTINGS_MODULE'] = 'settings.test'
from settings import settings
from monitoring.db import get_db


@pytest.fixture(scope="function", autouse=True)
async def clear_cache():
    # block before test

    yield clear_cache
    # called after test
    redis = await get_db()

    # удаляем данные
    await redis.flushall()
    return
    pattern = '{prefix}*'.format(prefix=settings.CACHE_PREFIX)  # CACHE_PREFIX removed, we flush db now
    keys = await redis.keys(pattern)
    if keys:
        await redis.delete(*keys)

    await redis.delete('my-key')  # manually delete


@pytest.fixture(scope="function", autouse=True)
async def reset_settings():
    redis = await get_db()

    # reset settings after test to initial value
    # block before test
    # get initial values to use them to restore initial settings after
    REDIS_EXPIRE = settings.REDIS_EXPIRE
    SNMP_NOTIFICATIONS_ENABLE = settings.SNMP_NOTIFICATIONS_ENABLE
    yield reset_settings

    # called after test
    settings.REDIS_EXPIRE = REDIS_EXPIRE
    settings.SNMP_NOTIFICATIONS_ENABLE = SNMP_NOTIFICATIONS_ENABLE

    for i in range(16):
        redis = await get_db(i)
        # убиваем соединение
        redis.close()
        await redis.wait_closed()
