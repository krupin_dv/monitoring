import os
import time
import asyncio
from datetime import timedelta
from datetime import datetime
from pprint import pprint

import pytest
from aiohttp import web

#import asynctest
#from asynctest import MagicMock, CoroutineMock

from settings import settings
from monitoring.app import get_app
from monitoring.db import get_db, redis_set, redis_get, redis_exists
from monitoring.views import CAMERA_COMPONENT_PATTERN
from monitoring.tests.test_components import component_events

@pytest.fixture
async def component_data():
    redis = await get_db()

    ## add test data
    await redis_set('some-not-needed-key', 'val')  # some not needed

    # save camera component
    cam_id = 123
    key = CAMERA_COMPONENT_PATTERN.format(cam_id=cam_id, component='mjpeg')
    data = {
        'fps': 29.5,
        'gpuid': '123456',
        'ts': time.time()
    }
    await redis.hmset_dict(key, data)
    return data
    # убиваем соединение
    redis.close()
    await redis.wait_closed()


@pytest.fixture
async def gpu_component_data():
    data = [
        {'id': 1,
         'inference': {
            'fps': 29.5,
            'gpuid': 1,
            'ts': time.time()
         }},
        {'id': 2,
         'inference': {
            'fps': 29.5,
            'gpuid': 1,
            'ts': time.time()
         }},
        {'id': 3,
         'inference': {
            'fps': 29.5,
            'gpuid': 1,
            'ts': time.time()
         }},
        {'id': 4,
         'inference': {
            'fps': 29.5,
            'gpuid': 2,
            'ts': time.time()
         }},
        {'id': 5,
         'inference': {
            'fps': 29.5,
            'gpuid': 2,
            'ts': time.time()
         }},
        {'id': 6,
         'inference': {
            'fps': 29.5,
            'gpuid': 2,
            'ts': time.time()
         }},
        {'id': 7,
         'inference': {
            'fps': 29.5,
            'gpuid': 3,
            'ts': time.time()
         }},
        {'id': 8,
         'inference': {
            'fps': 29.5,
            'gpuid': 3,
            'ts': time.time()
         }},
        {'id': 10,
         'inference': {
             'fps': 29.5,
             #'gpuid': 3,   # no gpu data
             'ts': time.time()
         }},
        {'id': 11,
         'inference': {
             'fps': 29.5,
             # 'gpuid': 3,   # no gpu data
             'ts': time.time()
         }},
    ]
    redis = await get_db()

    for camera_data in data:
        cam_id = camera_data['id']
        component = 'inference'
        inference_data = camera_data['inference']
        key = CAMERA_COMPONENT_PATTERN.format(cam_id=cam_id, component=component)
        await redis.hmset_dict(key, inference_data)

    # убиваем соединение
    redis.close()
    await redis.wait_closed()
    return data


async def test_inference_aggregate_gpu(aiohttp_client, loop, gpu_component_data):
    assert len(gpu_component_data) > 0

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['inference_aggregate_gpu'].url_for()

    resp = await client.get(url)
    assert resp.status == 200
    data = await resp.json()
    pprint(data)

    assert data['cam_count'] == len(gpu_component_data)
    assert data['gpu_count'] == 3
    assert data['no_gpu_count'] == 2
    assert len(data['result']) == data['gpu_count']
    assert len(data['no_gpu_result']) == data['no_gpu_count']


async def test_get_statuses(aiohttp_client, loop, component_data):
    cam_id = 123

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['get_statuses'].url_for()

    resp = await client.get(url)
    assert resp.status == 200
    data = await resp.json()
    pprint(data)
    assert data['count'] == 1
    assert data['result'][0]['id'] == cam_id  # first cam
    assert data['result'][0]['mjpeg']['fps'] == component_data['fps']
    assert data['result'][0]['mjpeg']['ts'] == component_data['ts']
    assert data['result'][0]['mjpeg']['is_alive'] is True

    # check COMPONENT_LIFE_TIME
    settings.COMPONENT_LIFE_TIME = 1
    await asyncio.sleep(1)

    resp = await client.get(url)
    assert resp.status == 200
    data = await resp.json()
    pprint(data)
    assert data['result'][0]['mjpeg']['is_alive'] is False


async def test_update_with_wrong_json(aiohttp_client, loop):
    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['update_status'].url_for(cam_id='123', component='mjpeg')

    resp = await client.post(url, data='')
    assert resp.status == 200
    data = await resp.json()
    pprint(data)


async def test_update_with_wrong_data(aiohttp_client, loop):
    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['update_status'].url_for(cam_id='123', component='mjpeg')


    resp = await client.post(url, json={'id': 'should be int', 'gpuid': 'should be int', 'fps': 'shoul be float'})
    assert resp.status == 400
    data = await resp.json()
    #pprint(data)
    assert 'errors' in data
    assert len(data['errors']) == 3


async def test_update_without_data(aiohttp_client, loop):
    redis = await get_db()
    cam_id = 123

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['update_status'].url_for(cam_id=str(cam_id), component='mjpeg')

    resp = await client.post(url, json={})
    assert resp.status == 200
    data = await resp.json()
    pprint(data)

    # check that data added to redis
    key = CAMERA_COMPONENT_PATTERN.format(cam_id=123, component='mjpeg')
    data = await redis.hgetall(key)
    print(data)


async def test_update_status(aiohttp_client, loop):
    redis = await get_db()
    cam_id = 123

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['update_status'].url_for(cam_id='123', component='mjpeg')

    post_data = {'fps': 29.5, 'gpuid': '1234567890', 'resolution': '1280x720'}
    resp = await client.post(url, json=post_data)
    assert resp.status == 200
    data = await resp.json()
    pprint(data)

    ## check that data added to redis
    key = CAMERA_COMPONENT_PATTERN.format(cam_id=123, component='mjpeg')
    data = await redis.hgetall(key)
    print(data)
    assert float(data['fps']) == post_data['fps']
    assert data['gpuid'] == post_data['gpuid']
    assert data['ts'] is not None  # timestamp here

    # check update
    url = app.router['update_status'].url_for(cam_id='123', component='inference')
    resp = await client.post(url, json=post_data)
    data = await resp.json()
    pprint(data)
    assert resp.status == 200


async def test_update_status_check_component_update(aiohttp_client, loop, component_data):
    redis = await get_db()
    cam_id = 123

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['update_status'].url_for(cam_id=str(cam_id), component='mjpeg')

    post_data = {'gpuid': '99999999'}
    resp = await client.post(url, json=post_data)
    assert resp.status == 200
    data = await resp.json()
    pprint(data)
    #assert 'mjpeg' in data

    ## check that data added to redis
    key = CAMERA_COMPONENT_PATTERN.format(cam_id=123, component='mjpeg')
    updated_component = await redis.hgetall(key)
    print(updated_component)
    #assert updated_component['id'] == cam_id
    assert updated_component['gpuid'] == post_data['gpuid']  # updated
    assert float(updated_component['fps']) == component_data['fps']  # should not changed
    assert float(updated_component['ts']) > component_data['ts']  # updated


async def test_update_status_1(aiohttp_client, loop):
    # component name with "-"
    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['update_status'].url_for(cam_id='123', component='kurento-pipeline')

    post_data = {}
    resp = await client.post(url, json=post_data)
    print(resp.status)
    data = await resp.json()
    pprint(data)
    assert resp.status == 200
    #assert 'kurento-pipeline' in data


async def test_cam_delete_view(aiohttp_client, loop, component_data):
    redis = await get_db()

    cam_id = 123
    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['cam_delete_view'].url_for(cam_id=str(cam_id))

    # check initial
    key = CAMERA_COMPONENT_PATTERN.format(cam_id=123, component='mjpeg')
    assert await redis.exists(key) == 1

    # check deletion
    resp = await client.delete(url)
    data = await resp.json()
    pprint(data)
    assert resp.status == 200
    assert 'deleted_records' in data
    assert data['deleted_records'] == 1
    assert await redis.exists(key) == 0

    assert await redis_exists('some-not-needed-key') == 1  # check that other data is not deleted

    # check response (deleted_records)
    resp = await client.delete(url)
    assert resp.status == 200
    data = await resp.json()
    assert data['deleted_records'] == 0


async def test_component_event_views__when_empty_id_param(aiohttp_client, loop, component_events):

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['event-list'].url_for(component='inference').with_query(id='[]')

    resp = await client.get(url)
    data = await resp.json()
    assert resp.status == 200
    assert data == {}


async def test_component_event_views__with_wrong_id_param(aiohttp_client, loop):

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['event-list'].url_for(component='inference').with_query(id='not json list')

    resp = await client.get(url)
    data = await resp.json()
    assert resp.status == 400
    assert 'error' in data


async def test_component_event_views(aiohttp_client, loop, component_events):

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['event-list'].url_for(component='inference').with_query(id='[1,2,3]')

    resp = await client.get(url)
    data = await resp.json()
    #pprint(data)
    assert resp.status == 200
    assert data['2'] == []
    assert data['3'] == []
    assert len(data['1']) == 3


async def test_component_event_views__with_simply_integer(aiohttp_client, component_events):

    app = get_app()
    client = await aiohttp_client(app)
    url = app.router['event-list'].url_for(component='inference').with_query(id='1')

    resp = await client.get(url)
    data = await resp.json()
    assert resp.status == 200
    assert len(data) == 1
    assert '1' in data
