from settings import settings

from monitoring.camera import update_camera
from monitoring.notification import send_notification


async def test_camera_not_alive_notification_snmp(aiohttp_client, loop):
    cam_id = 1
    component = 'kurento-pipeline'
    now_ts = 1000
    not_alive_ts = 500
    alive_ts = 400

    await update_camera({
        'id': cam_id,
        'name': 'Камера 1',
        'address': 'ываывава',
        'rtsp_uri_main': 'https://172.16.0.50/впввапвап'
    })

    settings.SNMP_NOTIFICATIONS_ENABLE = True
    res = await send_notification(cam_id, component, now_ts, not_alive_ts, alive_ts)
