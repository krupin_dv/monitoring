import logging
import traceback

import aiohttp
from aiohttp.client_exceptions import ClientConnectorError, ServerTimeoutError

from settings import settings


log = logging.getLogger('monitoring.views')


async def get_cameras_information():
    url = f'http://{settings.REST_SERVER_HOST}/api/video/object-observations/cameras/'
    headers = {'Request-Src': 'box-monitoring'}  # передав этот заголовок получим доступ ко всем камерам

    data = []  # return empty if some problems
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url, headers=headers) as resp:
                if resp.status == 200:
                    data = await resp.json()
                else:
                    text = await resp.text()
                    log.error(f'Rest server request error. Status: {resp.status}, Response: "{text}"')
    except ClientConnectorError:
        log.error('Rest server not available')
    except ServerTimeoutError:
        log.error('Rest server Timeout error')
    except Exception as e:
        tb = traceback.format_exc()
        log.error(f'Rest server request error. Exception: {e}')
        log.error(tb)

    return data
