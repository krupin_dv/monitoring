from monitoring.views import update_status, get_statuses, cam_delete_view, inference_aggregate_gpu
from monitoring.views import component_event_views


def setup_routes(app):
    app.router.add_get(r'/', get_statuses)  # cam list
    app.router.add_get(r'/api/camera/', get_statuses, name='get_statuses')  # cam list
    app.router.add_post(r'/api/camera/{cam_id:\d+}/component/{component:[\w\-]+}/detected/', update_status, name='update_status')
    app.router.add_delete(r'/api/camera/{cam_id:\d+}/', cam_delete_view, name='cam_delete_view')

    app.router.add_get(r'/api/camera/inference/aggregate-gpu/', inference_aggregate_gpu, name='inference_aggregate_gpu')

    # events
    app.router.add_get(r'/api/component/{component:[\w\-]+}/events/', component_event_views, name='event-list')
