import asyncio
import logging

from settings import settings

from monitoring.component_status import remove_not_alived_elements, remove_old_events
from monitoring.camera import update_cameras_information_task


log = logging.getLogger('monitoring.worker')


async def remove_not_alived_elements_worker():
    log.info('remove_not_alived_elements_worker: started')

    # функция remove_not_alived_elements при изменения состояния на неживое и отправляет уведомления
    # перезапуск сервиса может занять несколько минут, поэтому дадим время компонентам ожить
    log.debug(f'remove_not_alived_elements_worker: wait {settings.COMPONENT_LIFE_TIME} seconds')
    await asyncio.sleep(settings.COMPONENT_LIFE_TIME)  # ждём при первом запуске

    while True:
        log.debug('remove_not_alived_elements_worker: iteration')
        for component in settings.COMPONENTS:
            dead_elements = await remove_not_alived_elements(component)
            if dead_elements:
                log.info('{} elements become not alived in {}'.format(len(dead_elements), component))

        await asyncio.sleep(0.9)


async def remove_old_events_worker():
    log.info('<<< remove_old_events_worker started >>>')

    while True:
        deleted_count = await remove_old_events()
        if deleted_count:
            log.info(f'{deleted_count} events was deleted')

        await asyncio.sleep(60)  # запускаем раз минуту


async def update_cameras_information_worker():
    log.info('<<< update_cameras_information_worker started >>>')

    if not settings.SNMP_NOTIFICATIONS_ENABLE:
        # информация о камерах сейчас нужна только для snmp уведомлений
        log.info('update_cameras_information_worker stopped because SNMP notifications is disabled')
        return

    while True:
        log.info('<<< update_cameras_information_task started >>>')
        camera_count = await update_cameras_information_task()
        log.info(f'{camera_count} was updated')

        await asyncio.sleep(60*60)  # запускаем раз в час


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(remove_not_alived_elements_worker())
    loop.create_task(remove_old_events_worker())
    loop.create_task(update_cameras_information_worker())
    loop.run_forever()
