import os
import sys
import logging

from aiohttp import web

from settings import settings
from monitoring.routes import setup_routes

logger = logging.getLogger()
app = None


def get_app():
    global app
    is_test_env = getattr(settings, 'IS_TEST_ENV', False)
    # для каждого теста создаётся новый loop, поэтому нужен новый экземпляр
    if not app or is_test_env:
        app = web.Application()
        setup_routes(app)

    return app


if __name__ == '__main__':
    app = get_app()
    web.run_app(app)
