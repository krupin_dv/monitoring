from settings import settings
from monitoring.snmp import camera_not_alive_notification_snmp
from monitoring.camera import Camera, get_camera


async def send_notification(cam_id, component, now_ts, not_alive_ts, alive_ts):
    # send snmp notifications

    if settings.SNMP_NOTIFICATIONS_ENABLE:
        if component == 'kurento-pipeline':  # по этому компоненту определяем, что камера не работает
            # берём информацию из БД по камеры
            camera = await get_camera(cam_id)
            # если записи нет (возможно её ещё не загрузил worker) используем пустой объект
            if not camera:
                camera = Camera(cam_id)
            camera_not_alive_notification_snmp(camera, now_ts, not_alive_ts, alive_ts)
