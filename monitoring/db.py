import asyncio
import json

import aioredis

from settings import settings

is_test_env = getattr(settings, 'IS_TEST_ENV', False)
redis_connections = {}  # one connection for each db


async def get_db(db=0):
    global redis_connections
    redis_conn = redis_connections.get(db)
    if not redis_conn or is_test_env:
        redis_connections[db] = await aioredis.create_redis_pool((settings.REDIS_HOST, settings.REDIS_PORT), db=db, encoding='utf-8')
    return redis_connections[db]


async def redis_set(key, data, expire=0):
    redis = await get_db()
    key = key
    if expire == 0:
        expire = settings.REDIS_EXPIRE

    if not isinstance(data, (str, int, float, bytes, bytearray)):
        data = json.dumps(data)

    return await redis.set(key, data, expire=expire)


async def redis_get(key, try_json_decode=True):
    redis = await get_db()
    key = key
    val = await redis.get(key)
    if val and try_json_decode:
        try:
            val = json.loads(val)
        except json.decoder.JSONDecodeError:
            pass

    return val


async def redis_delete(key):
    redis = await get_db()
    key = key
    return await redis.delete(key)


async def redis_exists(key):
    redis = await get_db()
    key = key
    return await redis.exists(key)
