import logging

from pysnmp.hlapi import *

from settings import settings


log = logging.getLogger('monitoring.snmp')


def camera_not_alive_notification_snmp(camera, now_ts, not_alive_ts, alive_ts):

    log.info(f'Send SNMP notification: "No signal from camera {camera.id}"')

    camera_name = camera.name.encode()
    address = camera.address.encode()
    rtsp_uri_main = camera.rtsp_uri_main.encode()

    sys_descr = f'No signal from camera {camera.id} "{camera_name}"'
    sys_location = camera.id  #  'sysLocation' The physical location of this node (e.g., `telephone closet, 3rd floor').
    sys_up_time = alive_ts  # 'sysUpTime'  The time (in hundredths of a second) since the network management portion of the system was last re-initialized.

    # 'sysObjectID' The vendor's authoritative identification of the network management subsystem contained in the entity.
    # 'sysContact'  The textual identification of the contact person for this managed node, together with information on how to contact this person.

    errorIndication, errorStatus, errorIndex, varBinds = next(
        sendNotification(
            SnmpEngine(OctetString(hexValue='8000000001020304')),
            CommunityData('statserver'),  # 'public' by default
            UdpTransportTarget((settings.SNMP_HOST, settings.SNMP_PORT)),
            ContextData(),
            'trap',  # trap or inform
            [
                ObjectType(ObjectIdentity('SNMPv2-MIB', 'sysLocation'), OctetString(sys_location)),
                ObjectType(ObjectIdentity('SNMPv2-MIB', 'sysDescr'), OctetString(sys_descr)),
                ObjectType(ObjectIdentity('SNMPv2-MIB', 'sysUpTime'), Integer32(sys_up_time)),
                ObjectType(ObjectIdentity('1.3.6.1.2.3.0.1'), OctetString(rtsp_uri_main)),
                ObjectType(ObjectIdentity('1.3.6.1.2.3.0.3'), OctetString(camera_name)),  # тут вроде регион нужно указать
                ObjectType(ObjectIdentity('1.3.6.1.2.3.0.4'), OctetString(address)),
                ObjectType(ObjectIdentity('1.3.6.1.2.3.0.5'), OctetString('no signal')),
            ]
        )
    )

    if errorIndication:
        log.info(errorIndication)
    elif errorStatus:
        log.info('%s at %s' % (errorStatus.prettyPrint(),
                            errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
    else:
        for varBind in varBinds:
            log.info(' = '.join([x.prettyPrint() for x in varBind]))

    log.info('>>> end')
