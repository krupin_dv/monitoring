import asyncio
import time

from settings import settings
from monitoring.db import get_db
from monitoring.notification import send_notification


COMPONENT_STATE_DB = 1
COMPONENT_EVENT_DB = 2  # тут хранятся события по компонентам,

# храним данные в виде {ts1: cam_id1, ts2: cam_id2}
COMPONENT_ALIVE_PATTERN = '{component}-alive'
COMPONENT_EVENT_PATTERN = '{component}-{cam_id}'


def get_message_id_from_ts(ts):
    # 1600123458.123456 -> 1600123458.1234560  # append zeros to end if needed
    message_id = str(ts)
    int_part, float_part = message_id.split('.')
    if len(float_part) < 7:
        float_part = float_part + ('0' * (7 - len(float_part)))
    return int_part + float_part


async def component_update_signal(cam_id, component, ts):
    """ Вызываем когда компонет был обновлён
        функция записывает время когда был обновлён компонент
    """

    redis = await get_db(db=COMPONENT_STATE_DB)
    key = COMPONENT_ALIVE_PATTERN.format(component=component)
    res = await redis.zadd(key, ts, cam_id)  # zadd return number of elements added

    if res:  # значит элемент был добавлен, т.е. его не было, т.е. компонент "ожил"
        # создаём событие о том, что изменилось состояние на alived
        await write_event(cam_id, component, ts, {'is_alive': 1})

    return res


async def remove_not_alived_elements(component):

    loop = asyncio.get_event_loop()
    redis = await get_db(db=COMPONENT_STATE_DB)

    now_ts = time.time()
    ts_treshold = now_ts - settings.COMPONENT_LIFE_TIME
    key = COMPONENT_ALIVE_PATTERN.format(component=component)

    dead_elements = await redis.zrangebyscore(key, max=ts_treshold, withscores=True)
    for e in dead_elements:
        # создаём событие о том, что изменилось состояние на not_alived
        cam_id, alive_ts = e
        not_alive_ts = alive_ts + settings.COMPONENT_LIFE_TIME  # точное время с какого компонент считается неработающим
        await write_event(cam_id, component, not_alive_ts, {'is_alive': 0})
        # посылаем уведомления
        coro = send_notification(cam_id, component, now_ts, not_alive_ts, alive_ts)
        loop.create_task(coro)

    if dead_elements:
        # удаляем старые
        # выше мы создавали события, за это время некоторые елементы могли "ожить" (изменился ts)
        # поэтому они не удалятся, но это не важно
        await redis.zremrangebyscore(key, max=ts_treshold)

    return dead_elements


async def write_event(cam_id, component, ts, event_data):

    """
    Сохраняем событие
    для каждого компоненты камеры свой stream (т.е. отдельный ключ в БД)
    функция возрващает ид записи
    """

    assert isinstance(event_data, dict)
    assert len(event_data) > 0

    redis = await get_db(db=COMPONENT_EVENT_DB)
    stream = COMPONENT_EVENT_PATTERN.format(cam_id=cam_id, component=component)
    message_id = get_message_id_from_ts(ts)
    res = await redis.xadd(stream, event_data, message_id)
    return res


async def get_events(cam_id, component):

    redis = await get_db(db=COMPONENT_EVENT_DB)
    stream = COMPONENT_EVENT_PATTERN.format(cam_id=cam_id, component=component)
    res = await redis.xrange(stream)
    result = []
    for r in res:
        message_id, data = r
        ts_str_without_dot = message_id.split('-')[0]  # "16000902079854183-0" -> 16000902079854183
        ts = int(ts_str_without_dot) / 10000000  # 16000902079854183 -> 1600090207.9854183
        result.append({'ts': ts, 'is_alive': int(data['is_alive'])})

    return result


async def remove_old_events():

    """
    Проходим по всем логам в БД и удаляем старые события
    """

    redis = await get_db(db=COMPONENT_EVENT_DB)

    ts_treshold = time.time() - settings.COMPONENT_EVENT_EXPIRE
    message_id = get_message_id_from_ts(ts_treshold)

    delete_count = 0

    cursor = 0
    while True:
        res = await redis.scan(cursor)
        cursor, keys = res
        for stream_key in keys:
            # удаляем n старых евентов из стрима
            res = await redis.xrange(stream_key, start='-', stop=message_id, count=10)
            for message_id_to_remove, event_data in res:
                res = await redis.xdel(stream_key, message_id_to_remove)
                delete_count += 1

        if cursor == 0:
            break

    return delete_count
