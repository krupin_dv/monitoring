from monitoring.db import get_db
from monitoring.utils import get_cameras_information


CAMERA_INFORMATION_DB = 3  # в этой БД храним информацию о камерах, в качестве ключей используются ид камер


class Camera:
    id: int = None
    name: str = ''
    address: str = ''
    rtsp_uri_main: str = ''

    def __init__(self, id, name='', address='', rtsp_uri_main=''):
        self.id = int(id)
        self.name = name
        self.address = address
        self.rtsp_uri_main = rtsp_uri_main

    async def save(self):
        redis = await get_db(db=CAMERA_INFORMATION_DB)
        key = self.id
        res = await redis.hmset_dict(key, self.__dict__)
        return self


async def update_cameras_information_task():
    camera_count = 0
    data = await get_cameras_information()
    for oo in data:  # oo - объекта наблюдения, содержит поле 'cameras'
        for c in oo['cameras']:
            camera = Camera(
                id=c['id'],
                name=c['name'],
                address=oo['address'],  # адрес берём у объекта наблюдения
                rtsp_uri_main=c.get('rtsp_uri_main', ''),  # содержит ip-адрес камеры
            )
            await camera.save()
            camera_count += 1

    return camera_count


async def update_camera(data):
    """ Сохраняем данные в БД, возвращаем объект Camera"""
    camera = Camera(**data)
    await camera.save()
    return camera


async def get_camera(cam_id):
    redis = await get_db(db=CAMERA_INFORMATION_DB)
    key = cam_id
    camera = await redis.hgetall(key)  # get {} if not exists
    if not camera:
        return None

    return Camera(**camera)  # return object
