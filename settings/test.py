import os

from .base import *
IS_TEST_ENV = True
REDIS_HOST = os.getenv('REDIS_HOST', 'localhost')

REST_SERVER_HOST = os.getenv('REST_SERVER_HOST', '127.0.0.1:8000')  # 127.0.0.1:8000

COMPONENTS = ['mjpeg', 'inference', 'kurento-pipeline']
