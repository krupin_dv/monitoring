import os
import importlib


ENVIRONMENT_VARIABLE = 'SETTINGS_MODULE'


class Settings(object):
    def __init__(self):
        self.settings_module = os.environ.get(ENVIRONMENT_VARIABLE, 'settings.dev')

        try:
            mod = importlib.import_module(self.settings_module)
        except ImportError as e:
            raise ImportError("Could not import settings '%s' (Is it on sys.path?): %s" % (self.settings_module, e))

        for setting in dir(mod):
            if setting.isupper():
                setattr(self, setting, getattr(mod, setting))

    def get_settings_name(self):
        return self.settings_module


settings = Settings()
