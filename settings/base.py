import os
import sys
import logging.config

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

COMPONENTS = ['inference', 'kurento-pipeline']
COMPONENT_LIFE_TIME = int(os.getenv('COMPONENT_LIFE_TIME', 60*60))
COMPONENT_EVENT_EXPIRE = int(os.getenv('COMPONENT_EVENT_EXPIRE', 2*24*60*60))

REDIS_HOST = os.getenv('REDIS_HOST', 'box-monitoring-redis')
REDIS_PORT = int(os.getenv('REDIS_PORT', 6379))
REDIS_EXPIRE = 7*24*60*60
CACHE_PREFIX = 'monitoring-'

REST_SERVER_HOST = os.getenv('REST_SERVER_HOST', 'box-rest')  # 127.0.0.1:8000

# SNMP notifications
SNMP_NOTIFICATIONS_ENABLE = os.getenv('SNMP_NOTIFICATIONS_ENABLE', 'FALSE').upper() == 'TRUE'
SNMP_HOST = os.getenv('SNMP_HOST', '10.8.4.225')
SNMP_PORT = int(os.getenv('SNMP_PORT', 162))

print()
print('settings:')
print('BASE_DIR:', BASE_DIR)
print('REDIS_HOST:', REDIS_HOST)
print('REDIS_PORT:', REDIS_PORT)
print('REDIS_EXPIRE:', REDIS_EXPIRE)

print('REST_SERVER_HOST:', REST_SERVER_HOST)

print('COMPONENTS:', COMPONENTS)
print('COMPONENT_LIFE_TIME:', COMPONENT_LIFE_TIME)
print('COMPONENT_EVENT_EXPIRE:', COMPONENT_EVENT_EXPIRE)
print()

print('snmp notification settings:')
print('SNMP_NOTIFICATIONS_ENABLE:', SNMP_NOTIFICATIONS_ENABLE)
print('SNMP_HOST:', SNMP_HOST)
print('SNMP_PORT:', SNMP_PORT)


LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(pathname)s:%(lineno)s: %(message)s'
        },
    },
    'handlers': {
        'stdout': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
        'stderr': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stderr',
        },
        'file': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.FileHandler',
            'filename': '/dev/null',  # Default is stderr
        },
    },
    'loggers': {
        '': {  # root logger
            'handlers': ['stdout', 'file',],
            'level': 'INFO',
            'propagate': False
        },
        'asyncio': {
            'handlers': ['stdout', 'file',],
            'level': 'INFO',
            'propagate': False
        },
        'aiohttp.server': {
            'handlers': ['stdout', 'file',],
            'level': 'INFO',
            'propagate': False
        },
        'monitoring.views': {
            'handlers': ['stdout', 'file',],
            'level': 'DEBUG',
            'propagate': False
        },
        'monitoring.worker': {
            'handlers': ['stdout', 'file', ],
            'level': 'INFO',
            'propagate': False
        },
        'monitoring.snmp': {
            'handlers': ['stdout', 'file', ],
            'level': 'INFO',
            'propagate': False
        },
    }
}
logging.config.dictConfig(LOGGING_CONFIG)
